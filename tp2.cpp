#include <iostream>
#include <opencv2/opencv.hpp>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <stdlib.h>
#include <unistd.h>
#include <vector>
#include <sys/types.h>
#include <dirent.h>
#include <stdio.h>
#include <cmath>
#include <string.h>
#include <fstream>

using namespace std;
using namespace cv;

const char *dirExamples = "coil-100";
const char *imageExtension = ".png";

vector<string> getExamples(const char *dir);
int isImageFile(char *f);
void getHistogram(char *file, float *histogrameArray);
void getDescriptionImg();
void kmeansProcessing();
void searchImg(char *nameImgQuery);
float calculateDistanceHistogram(float *his1, float *his2);
void updateCentre(float *histogramArray, int indexCurrent);
void initKmeans();


typedef struct {
    string name;
    float *histogramImage;
    int indexCluster;
}desImg;

int scale, totalPixel = 0, nbCluster;
desImg *desImgs;
int nbImg = 0;
float **histogramCentre;
int maxIndex = 0;

int nbElement;
float *histogramArrayTmp = new float[maxIndex];

int main(int argc, char ** argv){
    scale = atoi(argv[1]);
    nbCluster = atoi(argv[2]);
    maxIndex = scale * 3;
    getDescriptionImg();
    kmeansProcessing();
    cout<<"ok \n";

    /**ofstream outfile;
    outfile.open("output1.txt");
    for(int i = 0; i < nbImg; i++){
        outfile<<desImgs[i].indexCluster<<" "<<desImgs[i].name<<endl;
    }*/

    for(int i = 0; i < nbCluster; i++){
        char resultUrl[128];
        sprintf(resultUrl, "%s/%s%d%s", "output", "cluster", i, ".txt");
        ofstream outfile;
        outfile.open(resultUrl);
        for(int j = 0; j < nbImg; j++){
            if(desImgs[j].indexCluster == i){
                outfile<<desImgs[j].indexCluster<<" "<<desImgs[j].name<<endl;
            }
        }
    }

   /**delete desImgs;
   delete histogramCentre;
   delete nbElement;
   delete histogramArrayTmp;*/

   return 0;
}

vector<string> getExamples(const char *dir){
    DIR *dp;
    struct dirent *ep;
    vector<string> listFile;

    dp = opendir (dir);
    if (dp != NULL){
        while (ep = readdir (dp)){
            if(isImageFile(ep->d_name)){
                listFile.push_back(ep->d_name);
                nbImg++;
            }
        }
        (void) closedir (dp);
    }else{
        perror ("Couldn't open the directory");
    }
    return listFile;
}

int isImageFile(char *f){
    if(strstr(f, imageExtension)){
        return 1;
    }
    return 0;
}

void getDescriptionImg(){

    vector<string> listFile;
    listFile = getExamples(dirExamples);

    desImgs = new desImg[nbImg];

    for(int i = 0; i < nbImg; i++){
        desImgs[i].name =  "unknown";
        desImgs[i].histogramImage = new float[scale * 3];
        desImgs[i].indexCluster = -1;
    }

    int indexImg = 0;
    for (vector<string>::iterator it = listFile.begin() ; it != listFile.end(); ++it){
        char imageUrl[128];
        sprintf(imageUrl, "%s/%s", dirExamples, (*it).c_str());
        desImgs[indexImg].name = imageUrl;
        getHistogram(imageUrl, desImgs[indexImg].histogramImage);
        indexImg++;
    }
}

void getHistogram(char *file, float *histogramArray){
    Mat image = imread(file, CV_LOAD_IMAGE_UNCHANGED);
    int totalPixel = 0;
    int maxIndex = scale * 3;

    for (int i = 0; i < maxIndex; i++){
            histogramArray[i] = 0;
    }

    for (int i = 0; i < image.size().width; i++){
        for (int j = 0; j < image.size().height; j++){
            Vec3b pixel = image.at<Vec3b>(j, i);

            int r = (int)pixel.val[0]*scale / 256;
            int g = (int)pixel.val[1]*scale / 256;
            int b = (int)pixel.val[2]*scale / 256;

            histogramArray[r] += 1;
            histogramArray[g+scale] += 1;
            histogramArray[g+scale*2] += 1;
        }
    }

    totalPixel += image.size().width * image.size().height;
    for(int i = 0; i < maxIndex; i++){
        histogramArray[i] = histogramArray[i] / totalPixel;
    }
}

void kmeansProcessing(){
    initKmeans();
    int count = 0;
    while(true){
        cout<<"count next "<<count<<endl;
        bool isChange = false;
        for(int i = 0; i < nbImg; i++){
            float minDistance = 10000.0;
            int indexClusterCurrent = -1;
            for(int j = 0; j < nbCluster; j++){
                float distanceHis = calculateDistanceHistogram(desImgs[i].histogramImage, histogramCentre[j]);
                if(minDistance > distanceHis){
                    minDistance = distanceHis;
                    indexClusterCurrent = j;
                }
            }
            if(desImgs[i].indexCluster != indexClusterCurrent){
                desImgs[i].indexCluster = indexClusterCurrent;
                isChange = true;
            }
            
            //cout<<indexClusterCurrent<<endl;
        }

        if(!isChange) break;
        for(int i = 0; i < nbCluster; i++){
            cout<<"update "<<count<<endl;
            updateCentre(histogramCentre[i], i);
        }
        cout<<"\n=============================== \n";
        count++;
    }
    cout<<"finish \n";
}

void initKmeans(){
    histogramCentre = new float*[nbCluster];

    for(int i = 0; i < nbCluster; i++){
        histogramCentre[i] = new float[maxIndex];
        int randomIndexImage = rand() % nbImg;
        while(desImgs[randomIndexImage].indexCluster != -1){
            randomIndexImage = rand() % nbImg;
        }
        desImgs[randomIndexImage].indexCluster = i;
        histogramCentre[i] = desImgs[i].histogramImage;
    }
}

void updateCentre(float *histogramArray, int indexCurrent){
    cout<<"update centre "<<indexCurrent<<" ";
    for(int i = 0; i < maxIndex; i++){
        nbElement = 0;
        histogramArrayTmp[i] = 0;
    }
    for(int i = 0; i  < nbImg; i++){
        if(desImgs[i].indexCluster == indexCurrent){
            for(int j = 0; j < maxIndex; j++){
                histogramArrayTmp[j] += desImgs[i].histogramImage[j];
            }
            nbElement++;
        }
    }
    for(int i = 0; i < maxIndex; i++){
        if(nbElement != 0){
            histogramArray[i] = histogramArrayTmp[i] / nbElement;
        }
    }
}

float calculateDistanceHistogram(float *his1, float *his2){
    float a = 0;
    float b = 0;
    for(int i = 0; i < maxIndex; i++){
        a += abs(his1[i] - his2[i]);
        b += his1[i];
    }
    return a / b;
}
